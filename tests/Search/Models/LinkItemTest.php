<?php

namespace SMTest;

use PHPUnit\Framework\TestCase;
use Module\Search\Models\LinkItem;

class LinkItemTest extends TestCase
{

    public function testGetContent()
    {
        $linkItem = new LinkItem('http://example.com', '<a href="http://www.vva.org/domains/example">More ...</a>');
        $this->assertSame('<a href="http://www.vva.org/domains/example">More ...</a>', $linkItem->getContent());
    }

    public function testGetContent_with_empty_schema()
    {
        $linkItem = new LinkItem('http://example.com', '<a href="/domains/vva">More ...</a>');
        $this->assertSame('<a href="//example.com/domains/vva">More ...</a>', $linkItem->getContent());
    }

    public function testGetType()
    {
        $linkItem = new LinkItem('http://example.com', '<a href="/domains/vva">More ...</a>');
        $this->assertSame('link', $linkItem->getType());
    }

}