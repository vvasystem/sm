<?php

return [
    'instances' => [
        \PDO::class => new \PDO(
            \sprintf('mysql:dbname=%s;host=%s;port=3306', \MYSQL_DATABASE, \MYSQL_HOST),
            \MYSQL_USER,
            \MYSQL_PASSWORD,
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            ]
        ),
    ],
];