<?php

namespace Common\Application;

interface ResponseInterface
{

    public function setStatus(int $code);

    public function getStatus(): int;

    public function setBody(string $string);

    public function getBody(): string;

}