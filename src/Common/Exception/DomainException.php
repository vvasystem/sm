<?php

namespace Common\Exception;

use Common\Search\SearchModuleExceptionInterface;

class DomainException extends \DomainException implements SearchModuleExceptionInterface
{

}