<?php

namespace Common\Helper;

class ClassNameHelper
{

    /**
     * @param string|object $classNameOrObject
     * @return string
     */
    public static function getName($classNameOrObject): string
    {
        if (\is_object($classNameOrObject)) {
            $classNameOrObject = \get_class($classNameOrObject);
        }
        $arr = \explode('\\', $classNameOrObject);

        return \end($arr);
    }

    /**
     * @param string|object $classNameOrObject
     * @return string
     */
    public static function getModule($classNameOrObject): string
    {
        if (\is_object($classNameOrObject)) {
            $classNameOrObject = \get_class($classNameOrObject);
        }
        $arr = \explode('\\', $classNameOrObject);
        \array_shift($arr);
        \reset($arr);

        return \current($arr);
    }

}
