<?php

namespace Common\Application;

interface RequestInterface
{

    public function get($name = null, $default_value = null);

    public function post($name = null, $default_value = null);

}