CREATE TABLE IF NOT EXISTS `search_result` (
    `search_result_id` INT(11) NOT NULL AUTO_INCREMENT,
    `search_result_url` VARCHAR(1024) NOT NULL,
    `search_result_type` ENUM('link','img','text') NOT NULL,
    `search_result_items` TEXT NOT NULL,
    `search_result_number_of_items` INT(11) NOT NULL,
    `search_result_create_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`search_result_id`),
    KEY `idx_search_result_url` (`search_result_url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
