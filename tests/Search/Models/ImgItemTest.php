<?php

namespace SMTest;

use PHPUnit\Framework\TestCase;
use Module\Search\Models\ImgItem;

class ImgItemTest extends TestCase
{

    public function testGetContent_with_absolute_url()
    {
        $imgItem = new ImgItem('http://example.com', '<img src="http://example.com/example.png"/>');
        $this->assertSame('<img src="http://example.com/example.png"/>', $imgItem->getContent());
    }

    public function testGetContent_without_scheme()
    {
        $imgItem = new ImgItem('http://example.com', '<img src="/example.png"/>');
        $this->assertSame('<img src="//example.com/example.png"/>', $imgItem->getContent());
    }

    public function testGetType()
    {
        $imgItem = new ImgItem('http://example.com', '<img src="/example.png"/>');
        $this->assertSame('img', $imgItem->getType());
    }

}