<?php

namespace SMTest;

use PHPUnit\Framework\TestCase;
use Common\Exception\InvalidArgumentException;
use Module\Search\Services\Site;
use Module\Search\Services\ItemsFinder;

class ItemsFinderTest extends TestCase
{

    const EXAMPLE_COM_CONTENT = <<<HTML
<!doctype html>
<html>
<head>
    <title>GoBase</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
    body {
        background-color: #40f0f2;
        font-family: sans-serif;
        
    }
    a:link, a:visited {
        color: #39988f;
        text-decoration: none;
    }
    @media (max-width: 900px) {
        body {
            background-color: #fff;
        }
    }
    </style>    
</head>

<body>
<div>
    <h1>GoBase</h1>
    <p>United information space for buyers and equipment manufacturers, 
    where all participants of the industrial market will find the necessary information about product of interest.</p>
    <p><a href="http://www.vva.org/system/example">More ...</a></p>
</div>
</body>
</html>
HTML;


    public function testFind_text()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $resultCollection = $finder->find('http://example.com/', 'text', 'United information space');
        $this->assertCount(1, $resultCollection);
    }

    public function testFind_invalid_url()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid url `foo`bar`. ');
        $finder->find('foo`bar', 'link');
    }

    public function testFind_unsupported_type()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unsupported search subject type `unsupported-type`. Supported types are: img, link, text. ');
        $finder->find('http://example.com/', 'unsupported-type');
    }

    public function testFind_empty_text_pattern()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Search pattern is empty.');
        $finder->find('http://example.com/', 'text', '');
    }

    private function createCrawlerMock(): Site    {
        $site = $this->createMock(Site::class);
        $site->method('grabContent')->willReturn(self::EXAMPLE_COM_CONTENT);

        /**@var Site $site */
        return $site;
    }

}