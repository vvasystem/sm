# Test task for timeweb.ru #

Specification: see test_task.jpg. 

Original estimate: 8-16 hours, elapsed time: ~12 hours.
 
See [CHANGELOG](/CHANGELOG.md). 

## Used libraries and tools:
    
* docker-compose for orchestration 
* php-fpm + pdo_mysql extension
* web server nginx
* database mysql 
* composer for autoloading
* twig as template engine 
* jQuery for ajax requests and other interactive features
* jQuery Form Validator for client side validation
* bootstrap for styling

## Requirements

1. composer
2. docker 1.13.1+ with docker-compose
3. free 8081 port (can be changed in .env)

## Install and run:

1. `git clone https://vvasystem@bitbucket.org/vvasystem/sm.git`
2. `cd sm/`
3. `./bin/composer.sh install`
4. `docker-compose up -d`
5. `./bin/migrate.sh` ( if you have got the error SQLSTATE[HY000] [2002] Connection refused or Can't connect to MySQL server on 'mysql' (115), please try again later (it's normal because mysql not yet started))
6. Open application [http://127.0.0.1:8081](http://127.0.0.1:8081)

## Testing

Run 
`./bin/phpunit.sh`

Coverage report available in [tests/coverage](/tests/coverage/index.html) 
