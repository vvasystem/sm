<?php

namespace Module\Search\Services;

use Common\Exception\InvalidArgumentException;
use Module\Search\Models\BaseItem;
use Module\Search\Models\ItemsCollection;

class ItemsFinder
{

    const PATTERN_IMG = '/(<img\s.*?src.*?[>$])/im';
    const PATTERN_LINK = '/(<a\s.*href.*>.*(<\/a>))/im';

    protected $supportedTypes = [
        BaseItem::TYPE_IMG,
        BaseItem::TYPE_LINK,
        BaseItem::TYPE_TEXT,
    ];

    /** @var Site */
    private $site;

    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    public function find(string $url, string $type, string $pattern = null): ItemsCollection
    {
        $this->validateUrl($url);
        if (BaseItem::TYPE_TEXT === $type) {
            $this->validatePattern($pattern);
        }
        $subject = $this->execCurlRequest($url);
        switch ($type) {
            case BaseItem::TYPE_IMG:
                \preg_match_all(self::PATTERN_IMG, $subject, $matches);
                $results = $matches[0];
                break;

            case BaseItem::TYPE_LINK:
                \preg_match_all(self::PATTERN_LINK, $subject, $matches);
                $results = $matches[0];
                break;
            case BaseItem::TYPE_TEXT:
                \preg_match_all($this->getTextRegexp($pattern), $this->prepareTextSubject($subject), $matches);
                $results = [];
                foreach ($matches[0] as $row) {
                    $results[] = \str_replace($pattern, "<b>{$pattern}</b>", $row);
                }
                break;
            default:
                $message = \sprintf('Unsupported search subject type `%s`. Supported types are: %s. ', $type,
                    \implode(', ', $this->supportedTypes));
                throw new InvalidArgumentException($message);
                break;
        }

        return ItemsCollection::createFromRawData($url, $type, $results);
    }

    protected function validateUrl(string $url)
    {
        if (false === \filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException(\sprintf('Invalid url `%s`. ', $url));
        }
    }

    protected function validatePattern(string $pattern)
    {
        if (empty($pattern)) {
            throw new InvalidArgumentException('Search pattern is empty. ');
        }
    }

    protected function execCurlRequest(string $url)
    {
        return $this->site->grabContent($url);
    }

    protected function getTextRegexp(string $pattern): string
    {
        return '/(\s.{1,64}' . \preg_quote($pattern) . '.{1,64}\s)/im';
    }

    protected function prepareTextSubject($subject): string
    {
        return \mb_convert_encoding(\strip_tags($subject), 'utf-8');
    }

}
