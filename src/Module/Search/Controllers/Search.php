<?php

namespace Module\Search\Controllers;

use Common\Application\BaseController;
use Module\Search\Models\ItemsRepository;
use Module\Search\Services\ItemsFinder;

class Search extends BaseController
{

    /** @var ItemsFinder */
    private $itemsFinder;

    /** @var ItemsRepository */
    private $itemsRepository;

    public function __construct(ItemsFinder $itemsFinder, ItemsRepository $itemsRepository)
    {
        $this->itemsFinder = $itemsFinder;
        $this->itemsRepository = $itemsRepository;
    }

    public function actionIndex()
    {
        $url = $this->request->get('searchUrl');
        $type = $this->request->get('searchType');
        $text = $this->request->get('searchText');

        $itemsCollection = $this->itemsFinder->find($url, $type, $text);
        $this->itemsRepository->save($itemsCollection);

        return $this->render('index', ['itemType' => $type, 'items' => $itemsCollection,]);
    }

}