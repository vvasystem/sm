<?php

namespace Module\Search\Models;

class ItemsCollection extends \ArrayObject
{

    protected $url;
    protected $type;
    protected $id;
    protected $timestamp;

    protected $itemFactory;

    public function __construct(string $url, string $type, array $items, $id = null, string $timestamp = null)
    {
        $this->url = $url;
        $this->type = $type;
        $this->id = $id;
        $this->timestamp = $timestamp;
        parent::__construct($items);
    }

    public static function createFromRawData(string $url, string $type, array $itemsContent): ItemsCollection
    {
        $collection = new ItemsCollection($url, $type, []);

        foreach ($itemsContent as $content) {
            $collection->addRawItem($content);
        }

        return $collection;
    }

    protected function getItemFactory(): ItemFactory
    {
        if (empty($this->factory)) {
            $this->itemFactory = new ItemFactory();
        }
        return $this->itemFactory;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    public function addRawItem(string $content)
    {
        $item = $this->getItemFactory()->factory($this->type, $this->url, $content);
        $this->append($item);
    }

}