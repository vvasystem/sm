<?php

namespace Module\Search\Models;

use Common\Exception\DomainException;

class ItemsRepository
{

    /** @var \PDO */
    private $pdoConnection;

    public function __construct(\PDO $pdoConnection)
    {
        $this->pdoConnection = $pdoConnection;
    }

    public function getPagesTotalCounts(): array
    {
        $statement = $this->pdoConnection->query('SELECT 
					sr.search_result_url as url,
					SUM(sr.search_result_number_of_items) as `totalNumberOfItems`
					FROM search_result sr
					GROUP BY sr.search_result_url'
        );
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllByUrl(string $url): array
    {
        $statement = $this->pdoConnection->prepare('SELECT search_result_id FROM `search_result` WHERE `search_result_url` = :url');
        $statement->execute([':url' => $url]);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $collections = [];
        foreach ($rows as $i => $row) {
            $collection = $this->getCollectionById($row['search_result_id']);
            $collections[$i] = $collection;
        }
        return $collections;
    }

    public function getCollectionById($id): ItemsCollection
    {
        $statement = $this->pdoConnection->prepare('SELECT * FROM `search_result` WHERE `search_result_id` = :id limit 1');
        $statement->execute([':id' => $id]);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        if ($row) {
            $items = \unserialize($row['search_result_items']);
            if (false === $items) {
                throw new DomainException(\sprintf('Error unserialize item with ID %s', $id));
            }
            $collection = new ItemsCollection($row['search_result_url'], $row['search_result_type'], $items, $row['search_result_id'], $row['search_result_create_timestamp']);
            return $collection;
        }

        throw new DomainException(\sprintf('Items collection with id `%s` not found. ', $id));
    }

    public function save(ItemsCollection $itemsCollection)
    {
        $statement = $this->pdoConnection->prepare('INSERT INTO `search_result` SET 
				`search_result_url` = :url,
				`search_result_items` = :items,
				`search_result_type` = :type,
				`search_result_number_of_items` = :numberOfItems
				');

        $statement->execute([
            ':url'           => $itemsCollection->getUrl(),
            ':items'         => \serialize((array)$itemsCollection),
            ':type'          => $itemsCollection->getType(),
            ':numberOfItems' => $itemsCollection->count(),
        ]);
    }

}