<?php

namespace Common\Application;

use Common\Helper\ClassNameHelper;
use Twig\Environment as Twig;

abstract class BaseController
{

    /** @var Twig */
    protected $twig;

    /** @var Request */
    protected $request;

    /** @var Response */
    protected $response;

    public function setTemplateEngine(Twig $engine)
    {
        $this->twig = $engine;
    }

    public function getResponse(string $actionName, RequestInterface $request): ResponseInterface
    {
        $this->response = new Response();
        $this->request = $request;
        $action = $this->routeAction($actionName);
        if (\method_exists($this, $action)) {
            $this->response->setBody($this->{$action}());
        } else {
            $this->response->setStatus(Response::STATUS_CODE_NOT_FOUND);
        }
        return $this->response;
    }

    protected function routeAction(string $actionName)
    {
        return 'action' . \ucfirst($actionName);
    }

    protected function render(string $name, array $context = []): string
    {
        $moduleName = ClassNameHelper::getModule(static::class);
        $className = \strtolower(ClassNameHelper::getName(static::class));
        return $this->twig->render(\sprintf('%s/Views/%s/%s.html.twig', $moduleName, $className, $name), $context);
    }

}