<?php

namespace Module\Search\Controllers;

use Common\Application\BaseController;

class Main extends BaseController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

}