<?php

namespace Common\Application;

use Common\Exception\InvalidArgumentException;
use FreeElephants\DI\Injector;
use Twig\Environment as Twig;
use Twig\Loader\FilesystemLoader;

class ControllerFactory
{

    /** @var Injector */
    private $injector;

    public function __construct(Injector $injector)
    {
        $this->injector = $injector;
    }

    public function createController(string $moduleName, string $controllerName): BaseController
    {
        $controllerClassName =  \sprintf('Module\\%s\\Controllers\\%s', \ucfirst($moduleName), \ucfirst($controllerName));
        if (!\class_exists($controllerClassName)) {
            throw new InvalidArgumentException(\sprintf('Controller %s not found', $controllerName));
        }

        /**@var $controller BaseController */
        $controller = $this->injector->createInstance($controllerClassName);

        $controller->setTemplateEngine(new Twig(new FilesystemLoader([
            \APPLICATION_PATH . '/Module',
            \APPLICATION_PATH . '/Common/View'
        ]), ['debug' => true,]));

        return $controller;
    }

}