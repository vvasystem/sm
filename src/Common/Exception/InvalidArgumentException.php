<?php

namespace Common\Exception;

use Common\Search\SearchModuleExceptionInterface;

class InvalidArgumentException extends \InvalidArgumentException implements SearchModuleExceptionInterface
{

}