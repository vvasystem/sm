<?php

namespace Module\Search\Models;

class LinkItem extends BaseItem
{

    const PATTERN_SRC = '/href(?:[=\s]+["\'])(.*?)(?:["\'])/im';

    const TYPE = self::TYPE_LINK;

    public function getContent(): string
    {
        \preg_match(self::PATTERN_SRC, $this->content, $matches);
        $src = $matches[1];
        foreach (['http://', 'https://', '//'] as $schema) {
            if (0 === \stripos($src, $schema)) {
                $schemaExist = true;
            }
        }

        if (empty($schemaExist)) {
            $baseUrl = '//' . \parse_url($this->url, PHP_URL_HOST) . $src;
            return \str_replace($src, $baseUrl, $this->content);
        }

        return $this->content;
    }

}