<?php

namespace Module\Search\Controllers;

use Common\Application\BaseController;
use Module\Search\Models\ItemsRepository;

class Archive extends BaseController
{

    /** @var ItemsRepository */
    private $itemsRepository;

    public function __construct(ItemsRepository $itemsRepository)
    {
        $this->itemsRepository = $itemsRepository;
    }

    protected function actionIndex()
    {
        $pages = $this->itemsRepository->getPagesTotalCounts();
        return $this->render('index', ['pages' => $pages,]);
    }

    protected function actionDetails()
    {
        $url = $this->request->get('url');
        $items = $this->itemsRepository->getAllByUrl($url);
        return $this->render('details', ['url' => $url, 'items' => $items,]);
    }

    protected function actionItemsCollection()
    {
        $collectionId = $this->request->get('collectionId');
        $collection = $this->itemsRepository->getCollectionById($collectionId);

        $html = '';
        $iterator = $collection->getIterator();
        while ($iterator->valid()) {
            $html .= $this->render('item', [
                'itemContent' => $iterator->current(),
                'itemNumber' => $iterator->key() + 1,
                'itemType' => $collection->getType(),
            ]);
            $iterator->next();
        }

        return $html;
    }

}