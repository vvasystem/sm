$(function () {
    var errorMsgs = {
        badUrl: 'Please, enter URL',
        requiredFields: 'Please, fill required field',
    };
    $.validate({
        language: errorMsgs,
        form: '[name=search]',
        onSuccess: function () {
            var form = $('form[name=search]');
            $.get(form.attr('action') + '&' + form.serialize(), {}, function (data) {
                $('#resultArea').html(data);
            });
            return false;
        },
    });

    if ($('input[name=searchType][value=text]').prop('checked')) {
        $('#searchTextWrapper').show();
        $('[name=useTextSearch]').prop('checked', true)
    } else {
        $('#searchTextWrapper').hide();
        $('[name=useTextSearch]').prop('checked', false)
    }

    $('input[name=searchType]').on('change', function () {
        if ($('input[name=searchType][value=text]').prop('checked')) {
            $('#searchTextWrapper').show();
            $('[name=useTextSearch]').prop('checked', true);
        } else {
            $('#searchTextWrapper').hide();
            $('[name=useTextSearch]').prop('checked', false)
        }
    });
});