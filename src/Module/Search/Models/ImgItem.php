<?php

namespace Module\Search\Models;

class ImgItem extends BaseItem
{

    const PATTERN_SRC = '/src(?:[=\s]+["\'])(.*?)(?:["\'])/im';

    const TYPE = self::TYPE_IMG;

    public function getContent(): string
    {
        \preg_match(self::PATTERN_SRC, $this->content, $matches);
        $src = $matches[1];
        foreach (['http://', 'https://', '//'] as $schema) {
            if (0 === \stripos($src, $schema)) {
                $schemaExist = true;
            }
        }

        if (empty($schemaExist)) {
            $baseUrl = '//' . \parse_url($this->url, PHP_URL_HOST) . $src;
            return \str_replace($src, $baseUrl, $this->content);
        }

        return $this->content;
    }

}