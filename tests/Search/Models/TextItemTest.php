<?php

namespace SMTest;

use PHPUnit\Framework\TestCase;
use Module\Search\Models\TextItem;

class TextItemTest extends TestCase
{

    public function testGetUrl()
    {
        $textItem = new TextItem('http://example.com', 'Some text 1');
        $this->assertSame('http://example.com', $textItem->getUrl());
    }

    public function testGetType()
    {
        $textItem = new TextItem('http://example.com', 'Some text 2');
        $this->assertSame('text', $textItem->getType());
    }

    public function testGetContent()
    {
        $textItem = new TextItem('http://example.com', 'Some text 3');
        $this->assertSame('Some text 3', $textItem->getContent());
    }

    public function testToString()
    {
        $textItem = new TextItem('http://example.com', 'Some text 4');
        $this->assertSame('Some text 4', (string) $textItem);
    }

}