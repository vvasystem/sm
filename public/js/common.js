$(function () {
    $('a.ajax').on('click', function () {
        var targetBlock = $(this).attr('target');
        $.get($(this).attr('href'), {}, function (data) {
            $(targetBlock).html(data);
        });
        return false;
    });
});