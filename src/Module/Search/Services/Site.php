<?php

namespace Module\Search\Services;

use Common\Exception\RuntimeException;

class Site
{

    public function grabContent(string $url): string
    {
        $ch = \curl_init($url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $subject = \curl_exec($ch);
        if (false === $subject) {
            $message = \sprintf('Can not connect to url `%s`. ', $url);
            throw new RuntimeException($message, \curl_errno($ch));
        }

        return $subject;
    }

}