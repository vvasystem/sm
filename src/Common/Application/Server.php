<?php

namespace Common\Application;

class Server
{

    const DEFAULT_MODULE_NAME     = 'search';
    const DEFAULT_CONTROLLER_NAME = 'main';
    const DEFAULT_ACTION_NAME     = 'index';

    /** @var ControllerFactory */
    private $controllerFactory;

    public function __construct(ControllerFactory $controllerFactory)
    {
        $this->controllerFactory = $controllerFactory;
    }

    public function processRequest(RequestInterface $request)
    {
        $moduleName = $request->get('m', self::DEFAULT_MODULE_NAME);
        $controllerName = $request->get('c', self::DEFAULT_CONTROLLER_NAME);
        try {
            $controller = $this->controllerFactory->createController($moduleName, $controllerName);
        } catch (\Throwable $e) {
            $response = new Response();
            $response->setStatus(Response::STATUS_CODE_NOT_FOUND);
            $this->sendResponse($response);
            return;
        }

        $actionName = $request->get('a', self::DEFAULT_ACTION_NAME);
        $response = $controller->getResponse($actionName, $request);

        $this->sendResponse($response);
    }

    protected function sendResponse(ResponseInterface $response)
    {
        \http_response_code($response->getStatus());
        echo $response->getBody();
    }

}