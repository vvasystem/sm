<?php

namespace SMTest;

use PHPUnit\Framework\TestCase;
use Common\Exception\RuntimeException;
use Module\Search\Services\Site;

class SiteTest extends TestCase
{

    public function testGrabContent_exception()
    {
        $crawler = new Site();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Can not connect to url `http://foo`bar`. ');
        $crawler->grabContent('http://foo`bar');
    }

}