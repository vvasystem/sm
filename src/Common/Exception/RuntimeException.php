<?php

namespace Common\Exception;

use Common\Search\SearchModuleExceptionInterface;

class RuntimeException extends \RuntimeException implements SearchModuleExceptionInterface
{

}