#!/bin/bash

#!/usr/bin/env bash

source .env

cat base_migration.sql | docker exec -i mysql \
    mysql -u ${MYSQL_USER} --password=${MYSQL_PASSWORD} ${MYSQL_DATABASE}