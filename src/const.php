<?php

const APPLICATION_PATH = __DIR__;

\define('MYSQL_HOST', \getenv('MYSQL_HOST'));
\define('MYSQL_USER', \getenv('MYSQL_USER'));
\define('MYSQL_PASSWORD', \getenv('MYSQL_PASSWORD'));
\define('MYSQL_DATABASE', \getenv('MYSQL_DATABASE'));
\define('MYSQL_CHARSET', \getenv('MYSQL_CHARSET'));
