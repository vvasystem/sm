<?php
require_once '../vendor/autoload.php';

use FreeElephants\DI\Injector;
use FreeElephants\DI\InjectorBuilder;
use Common\Application\Request;
use Common\Application\Server;

require __DIR__ . '/../src/const.php';

$components = require __DIR__ . '/../src/Common/components.php';
$di = (new InjectorBuilder())->buildFromArray($components);
$di->setService(Injector::class, $di);
$di->allowNullableConstructorArgs(true);
$di->allowInstantiateNotRegisteredTypes(true);
$di->registerItSelf();

$app = $di->getService(Server::class);
/**@var Server $app */
$app->processRequest(new Request($_SERVER, $_GET, $_POST));
