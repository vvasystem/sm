<?php

namespace SMTest;

use PHPUnit\Framework\TestCase;
use Module\Search\Models\ImgItem;
use Module\Search\Models\ItemFactory;
use Module\Search\Models\LinkItem;
use Module\Search\Models\TextItem;

class ItemFactoryTest extends TestCase
{

    public function testFactory()
    {
        $factory = new ItemFactory();

        $this->assertInstanceOf(ImgItem::class,  $factory->factory('img', 'http://example.com', '<img src="/vva.png"/>'));
        $this->assertInstanceOf(LinkItem::class,  $factory->factory('link', 'http://example.com', '<a href="http://www.vva.org/system/example">More...</a>'));
        $this->assertInstanceOf(TextItem::class,  $factory->factory('text', 'http://example.com', 'Some text'));
    }

}